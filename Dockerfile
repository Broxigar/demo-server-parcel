FROM node:8

RUN mkdir -p /server
WORKDIR /server
COPY . .

ENV MODE prod
ENV REACT_APP_MODE prod

RUN yarn install

WORKDIR /server/client

RUN yarn install
RUN yarn run build

WORKDIR /server

CMD ["npm", "start"]
