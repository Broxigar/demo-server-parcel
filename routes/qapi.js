var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');
var multer  = require('multer')
var dbServices = require('../moudle/db-service');
var passport = require('../config/passport');
var dateFormat = require('dateformat');

/* GET home page. */
router.get('/doc', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../client/build/index.html'));
});

router.get('/log', function(req,res,next){
  fs.createReadStream(path.join(__dirname, '../nohup.out'))
  .pipe(res);
});

var servicesMap = {};
function reloadService(){
  dbServices.services(function(err, data){
    for(var i=0; i<data.length; i++){
      // console.log(data[i].name);
      var api = data[i].API;
      var apiName = String(api.split('|')[0].trim());
      servicesMap[apiName] = data[i];
    }
  console.log("Loading servicesMap...");
  // console.log(servicesMap);
  });
}

reloadService();

router.get('/reload', function(req,res,next){
  reloadService();
  res.json({"refresh":"success"});
});


router.get('/searchIndex/:search', function(req,res,next){
  var s = Search(req.params.search);
  console.log(s);
  //res.json(s);
  res.json(s);
});  


router.get('/all', function(req, res, next) {
  // dbServices.services(function(err, data){
  // console.log(data);
  var data = Object.values(servicesMap);
  console.log("Print values:");
  console.log(data);
  objs = [];
  for(var i=0; i<data.length; i++){
    // console.log(data[i].API);
    obj = {};
    obj.id = data[i].id;
    objArray = data[i].API.split('|');
    obj.serviceName = objArray[0].trim();
    obj.sql = objArray[1].toUpperCase();
    obj.info = objArray[2];
    obj.params = [];
    obj.returns = [];
    obj.method = "";
    sqlArray = obj.sql.trim().split(/[,\n=\s+]+/);
    // console.log(sqlArray[0].toUpperCase());
    if(sqlArray[0].toUpperCase() === "SELECT") obj.method = "GET";
    if(sqlArray[0].toUpperCase() === "INSERT") obj.method = "POST";
    if(sqlArray[0].toUpperCase() === "DELETE") obj.method = "DELETE";
    if(sqlArray[0].toUpperCase() === "UPDATE") obj.method = "PUT";
    // console.log(sqlArray);
    for(var j=0; j<sqlArray.length; j++){

      if(sqlArray[j].toUpperCase()==='AS'){
        obj.returns.push(sqlArray[j+1]);
      }
      if(sqlArray[j] === "?"){
        obj.params.push(sqlArray[j-1]);
      }
    }
    // console.log(obj);
    objs.push(obj);
  }
  res.json(objs);
  // });
  // res.json(servicesMap);
});

router.get('/:service', function(req, res, next) {
  var serviceName = req.params.service.trim();

  if(servicesMap[serviceName]!=null && servicesMap[serviceName]!=undefined){
    var sql = servicesMap[serviceName].API.split('|')[1];
    dbServices.query(sql, function(error, dd){
      console.log(error);
      // console.log(dd);
      res.json(dd)
    });
  }else{
    console.log("Query service failed");
  }
});

router.get('/:service/:param1', function(req, res, next) {
  var serviceName = req.params.service.trim();

  if(servicesMap[serviceName]!=null && servicesMap[serviceName]!=undefined){
    var sql = servicesMap[serviceName].API.split('|')[1];
    dbServices.query(sql, [req.params.param1],function(error, dd){
      console.log(sql, req.params.param1)
      console.log(error);
      // console.log(dd);
      res.json(dd);
    });
  }else{
    console.log("Query service failed");
  }
});

router.get('/:service/:param1/:param2', function(req, res, next) {
  var serviceName = req.params.service.trim();
  if(servicesMap[serviceName]!=null && servicesMap[serviceName]!=undefined){
    var sql = servicesMap[serviceName].API.split('|')[1];
    console.log(req.params.param1, isNaN(req.params.param1))
    console.log(parseInt(req.params.param1))
    var param1 = isNaN(req.params.param1)? req.params.param1 : parseInt(req.params.param1)
    var param2 = isNaN(req.params.param2)? req.params.param2 : parseInt(req.params.param2)
    console.log(sql, param1, param2, typeof(param1), typeof(param2))
    dbServices.query(sql, [param1, param2],function(error, dd){
      console.log(error);
      // console.log(dd);
      res.json(dd);
    });
  }else{
    console.log("Query service failed");
  }
});

router.post('/:service',function(req, res) {
  var serviceName = req.params.service.trim();
  if(servicesMap[serviceName]!=null && servicesMap[serviceName]!=undefined){
    var sql = servicesMap[serviceName].API.split('|')[1];
    console.log(sql)
    // delete req.body[serviceName]
    console.log(req.body)
    dbServices.query(sql, [req.body],function(error, dd){
      console.log(error);
      // console.log(dd);
      if(error){
        res.status(201).json(error);
      } else {
        res.json(dd)
      }  
    });
  }else{
    console.log("Query service failed");
    res.status(500).json({"error":"internal error"})
  }
});


module.exports = router;
