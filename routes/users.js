var express = require('express');
var router = express.Router();
var passport = require('../config/passport');
var jwt = require('jsonwebtoken');
var exjwt = require('express-jwt');
var bcrypt = require('bcrypt-nodejs');
var config = require('../moudle/config');
var dbServices = require('../moudle/db-service');
//var passport = require('../config/googleAuth');
var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
  var passportJWT = require("passport-jwt");
  var ExtractJwt = passportJWT.ExtractJwt;
var elasticlunr = require('elasticlunr');


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/signin/:phone', function(req, res, next) {
  var sql = 'select IDUSER, IDSHOP, NAME, PHONE, EMAIL, ADDRESS, CODE, CODEEXPIRE, IMAGE,POSTLIMIT, TYPE, HIDDENEMAIL, HIDDENPHONE from USER where phone = ?';
    dbServices.query(sql, [req.params.phone], function(error, dd){

      if(!error){
        res.json(dd[0])
      }else{
        res.json(error)
      }
    });
});

router.post("/secret", passport.authenticate('jwt'), function(req, res){
  res.json("Success! You can not see this without a token");
});

router.post('/login',
  passport.authenticate('user'),
  function(req, res) {
    console.log(req.user, req.user_f)
    res.json(req.user);
  })


router.post('/adminlogin',
  passport.authenticate('admin'),
  function(req, res) {
    
    res.json(req.user);
  });

router.get('/logout', (req, res)=> {
  //console.log(JSON.parse(localStorage.getItem(req.user.email)));
  //localStorage.removeItem(req.user.email);
  //console.log(JSON.parse(localStorage.getItem(req.user.email)));
  //console.log("a: "+req.user);
  //localStorage.clear();
  req.logOut();
  res.json({'logout':'success'})
  // res.redirect('/');
  console.log('logout');
  //res.json({success: true, msg: 'Successful signed out.'})
  
});

router.post('/pw', (req, res)=> {
  bcrypt.genSalt(10, function (err, salt) {
    if (err) {
        res.json(err);
    }

    bcrypt.hash(req.body.name, salt, null, function (err, hash) {
        if (err) {
          res.json(err);
        }
        else {
          res.json({msg: hash});

        }
      })
    })
  
});

  
module.exports = router;
