/* eslint-env es6 */

// 'use strict';
const fs = require('fs')

var zipFiles = require('../moudle/zipFiles');

// This example is for testing Gmail actions with a Google Apps account
// https://developers.google.com/gmail/markup/overview

// see https://cloudup.com/chLuRjJy61U for example output of this script

const nodemailer = require('nodemailer');
// const nodemailerDkim = require('nodemailer-dkim');

// Gmail Actions are enabled by default for messages sent by the same user
// so this test script uses the same from: and to: addresses
const address = '314018042@qq.com';

var connection = {
    host: 'hwsmtp.exmail.qq.com',
    port: 465,
    secure: true,
    auth: {
        user: '',
        pass: ''
    },
    logger: true
};

// Create reusable transporter object using the default SMTP transport
var mail = nodemailer.createTransport(connection);

var req = {
    body: 
    { email: 'jevy.wangfei@gmail.com',
  xieshouName: 'Staff',
  name: 'test',
  status: 'waiting',
  urls: 
   [ { path: 'data/img/1559273612158-52009820190529167474.pdf',
       size: 136115,
       encoding: '7bit',
       filename: '1559273612158-52009820190529167474.pdf',
       mimetype: 'application/pdf',
       fieldname: 'file',
       fileclass: 'Marking/Assessment Criteria',
       destination: 'data/img/',
       originalname: '52009820190529167474.pdf' },
     { path: 'data/img/1559478820678-Birth registration statement.doc',
       size: 409088,
       encoding: '7bit',
       filename: '1559478820678-Birth registration statement.doc',
       mimetype: 'application/msword',
       fieldname: 'file',
       fileclass: 'Case Study',
       destination: 'data/img/',
       originalname: 'Birth registration statement.doc' },
     { path: 'data/img/1559478820692-suggestion.png',
       size: 89594,
       encoding: '7bit',
       filename: '1559478820692-suggestion.png',
       mimetype: 'image/png',
       fieldname: 'file',
       fileclass: 'Case Study',
       destination: 'data/img/',
       originalname: 'suggestion.png' },
     { path: 'data/img/1559478820693-updated_Feb.csv',
       size: 103971,
       encoding: '7bit',
       filename: '1559478820693-updated_Feb.csv',
       mimetype: 'text/csv',
       fieldname: 'file',
       fileclass: 'Case Study',
       destination: 'data/img/',
       originalname: 'updated_Feb.csv' },
     { path: 'data/img/1559478820696-Lecture 7 (1).pptx',
       size: 2937312,
       encoding: '7bit',
       filename: '1559478820696-Lecture 7 (1).pptx',
       mimetype: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
       fieldname: 'file',
       fileclass: 'Case Study',
       destination: 'data/img/',
       originalname: 'Lecture 7 (1).pptx' } ],
  textName: 'CustomerWords.txt',
  textContent: '' }
}
// setup e-mail data
var mailOptions = {
  from: "service@funtolearn.co",
  to: req.body.email,
  subject: req.body.name, // Subject line
  html: `<p>Hi ${req.body.xieshouName},</p>
  <br/>
  <p>A new order <h3>${req.body.name}</h3> has been assigned to you.</p>
  <p>You can login the <a href="https://console.funtolearn.co">Fun to Learn system</a> using your given email&passsword to get more detail.
  And you can do more operations on it.</p>
  <br/>
  <p>Download the compressed assignment materials directly: <a href="https://localhost:3000/api/assignment/${req.body.name}.zip">${req.body.name}</a>. 
  </p><p>The .zip file includes all materials customer required. </p>
  <p>Please contact Fun to Learn using wechat or replying this email if you have any question. </p>
  <br/>
  <p>Best Regard,</p>
  <p><a href="https://funtolearn.co">Fun to Learn Team</a></p>`
};

// send mail
if(!fs.existsSync('data/assignment/'+req.body.name+'.zip')){
  zipFiles(req.body.name, req.body.urls, req.body.textName, req.body.textContent, 'assignment')
}
// mail.sendMail(mailOptions);

module.exports = mail;



