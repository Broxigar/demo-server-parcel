var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors')
var path = require('path')

var api = require('./routes/api');
var qapi = require('./routes/qapi')
var users = require('./routes/users');

const WebSocket = require('ws');

var app = express();
var passport = require('passport'),
    session = require("express-session"),
    bodyParser = require("body-parser");

// view engine setup_Mingjing
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client/build')));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({ 
  secret: "anything", 
  resave: true,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json())   

// app.all('*', ensureSecure); // at top of routing calls
if(process.env.MODE==='dev'){
  app.use('/api', cors({allowedHeaders:"Origin, X-Requested-With, Content-Type, Authorization, Accept, Authorization"}), api);
  app.use('/qapi', cors({allowedHeaders:"Origin, X-Requested-With, Content-Type, Authorization, Accept, Authorization"}), qapi);
}else{
  app.use('/api', api);
  app.use('/qapi', qapi);
}

app.use('/users', users);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

const wss = new WebSocket.Server({server: app});

var clientMap = new Map();
var messageCacheMap = new Map();
wss.on('connection', function connection(ws) {
  // console.log(ws)
  // client = JSON.stringify(ws)
  // clientMap.set()
  ws.on('message', function incoming(message) {
    console.log(message)  
    var message = JSON.parse(message)
    if(message.type=='conn'){
      console.log(message.idUser + 'log on...')
      clientMap.set(message.idUser, ws)
      var hisMessage = messageCacheMap.get(message.idUser)
      if(hisMessage){
        ws.send(JSON.stringify(hisMessage))
      }

    }else{
      var friendWS = clientMap.get(message.to)
      if(friendWS){
        //customer online
        console.log('client '+message.to +
        ' on line, send message from '+ message.from + ' to ' + message.to)
        friendWS.send('['+JSON.stringify(message)+']')
      }else{
        console.log('client '+message.to + ' off line')
        //client offline, store message
        var hisMessage = messageCacheMap.get(message.to)
        if(hisMessage){
          hisMessage.push(message)
          messageCacheMap.set(message.to, hisMessage)
        }else{
          messageCacheMap.set(message.to, [message])
        }
      }
    }
    // console.log(clientMap)
    // console.log(messageCacheMap)
    // ws.send(message);
    // console.log('received: %s', message);
  });

  // ws.send('connected...');
});
  
module.exports = app;
